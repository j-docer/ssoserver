package study.pj.zuul.sso.server.util;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * 加密工具类
 */
public class PasswordUtil {

    public static String encodePwd(String pwd) {
        return new Md5Hash(pwd, Salt.SALT, 2).toString();
    }

    public static void main(String[] args) {

        System.out.println(encodePwd("admin"));


        System.out.println(encodePwd("user"));
        System.out.println(encodePwd("user2"));
    }
}
