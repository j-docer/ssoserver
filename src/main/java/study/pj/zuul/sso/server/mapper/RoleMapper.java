package study.pj.zuul.sso.server.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import study.pj.zuul.sso.server.model.Role;


@Mapper
@Component
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    String findPrivilege(Integer id);


}