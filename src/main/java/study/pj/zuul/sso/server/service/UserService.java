package study.pj.zuul.sso.server.service;

import study.pj.zuul.sso.server.model.User;

public interface UserService {
    User findByUserName(String userName);
}
