package study.pj.zuul.sso.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

/**
 * s_user
 *
 * @author
 */

@Data
@Alias("user")
@AllArgsConstructor
public class User implements Serializable {
    private Long id;

    private Integer roleId;

    private String username;

    private String password;

    private Date createDate;

    private static final long serialVersionUID = 1L;
}