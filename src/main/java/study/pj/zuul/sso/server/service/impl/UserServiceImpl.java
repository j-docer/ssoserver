package study.pj.zuul.sso.server.service.impl;

import org.springframework.stereotype.Service;
import study.pj.zuul.sso.server.mapper.UserMapper;
import study.pj.zuul.sso.server.model.User;
import study.pj.zuul.sso.server.service.UserService;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User findByUserName(String userName) {
        return userMapper.findByUserName(userName);
    }
}
