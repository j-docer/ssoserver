package study.pj.zuul.sso.server.controller;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import study.pj.zuul.sso.server.model.User;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.dc;

@RestController
public class TestController {

    @Autowired
    private StringRedisTemplate stringTemplate;


    @RequestMapping("/testSet")
    @ResponseBody
    public void testSEt() {
//        String[] values = new String[]{"AAA", "BBB", "CCC"};
//        String test = "testAAAAAA";
//        stringTemplate.opsForSet().add(test, values);
//        stringTemplate.expire(test, 10, TimeUnit.SECONDS);
//        System.out.println("set test");
        Boolean t = Boolean.FALSE;
        tttt(t);
        System.out.println(t);

    }

    private void tttt(Boolean t) {
        t = Boolean.TRUE;
    }


    //强制用户下线操作
    @GetMapping("/off")
    public ModelAndView offline() {
        // accessToken是新用户，因此，除了这个之外，其他的用户都要下线。
        ModelAndView modelAndView = new ModelAndView("offline");
        modelAndView.addObject("accessToken", "aada");
        modelAndView.addObject("url", "ssdasdfasdf");
        return modelAndView;
    }


    @RequestMapping("/testgetSet")
    @ResponseBody
    public Set<String> testgetSet() {
//        String[] values = new String[]{"AAA", "BBB", "CCC"};
        String test = "testAAAAAA";
//        stringTemplate.opsForSet().add(test, values);
        Set<String> d = stringTemplate.opsForSet().members(test);

        return d;
    }


}
