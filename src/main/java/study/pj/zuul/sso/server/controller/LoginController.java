package study.pj.zuul.sso.server.controller;

import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import study.pj.zuul.sso.server.mapper.UserMapper;
import study.pj.zuul.sso.server.model.User;
import study.pj.zuul.sso.server.service.UserService;
import study.pj.zuul.sso.server.util.PasswordUtil;
import study.pj.zuul.sso.server.util.UuidUtil;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Controller
public class LoginController {

    static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private StringRedisTemplate stringTemplate;

    @Resource
    private UserService userService;

    @Resource
    private UserMapper userMapper;


    // 测试页面能够进入
    @RequestMapping("/testin")
    @ResponseBody
    public String testin() {
        return " 翘课";
    }

    // 测试连接数据
    @RequestMapping("/testdb")
    @ResponseBody
    public String testdb() {
        User user = userMapper.findByUserName("admin");
        logger.info("user:" + user);
        return "DB-OK";
    }

    /**
     * 判断accessToken是否存在
     *
     * @param accessToken accessToken
     */
    @RequestMapping("/redis/hasKey/{key}")
    @ResponseBody
    public Boolean hasKey(@PathVariable("key") String accessToken) {
        try {
            logger.info("stringTemplate.hasKey(" + accessToken + ");" + stringTemplate.hasKey(accessToken));
            return stringTemplate.hasKey(accessToken);
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * 校验用户名密码，成功则返回通行令牌
     *
     * @param username
     * @param password
     * @param hasAlreadyLogin 是否已经有用户登陆过记录
     */
    @RequestMapping("/sso/checkUsernameAndPassword")
    private String checkUsernameAndPassword(String username, String password, LoginTag hasAlreadyLogin) {
        //通行令牌
        String accessToken = null;
        // 从数据库查询用户名和密码。
        String enyPwd = PasswordUtil.encodePwd(password);

        User user = userService.findByUserName(username);
        if (user != null) {
            if (username.equals(username) && enyPwd.equals(user.getPassword())) {
                // userid+盐生成一个loginUserToken，
                String userId = String.valueOf(user.getId());
                String loginUserTokenSet = PasswordUtil.encodePwd(userId);

                //用户名+时间戳（这里只是demo，正常项目的令牌应该要更为复杂）
                accessToken = username + UuidUtil.getUuid();

                //令牌作为key，存用户id作为value（或者直接存储可暴露的部分用户信息也行）设置过期时间（我这里设置10分钟）
                stringTemplate.opsForValue().set(accessToken, userId, (long) (10 * 60), TimeUnit.SECONDS);

                // 同一个账号的登陆用户集合
                if (stringTemplate.opsForSet().members(loginUserTokenSet).size() > 0) {
                    hasAlreadyLogin.setOn(Boolean.TRUE);
                }
                stringTemplate.opsForSet().add(loginUserTokenSet, accessToken);
                stringTemplate.expire(loginUserTokenSet, 30 * 60, TimeUnit.SECONDS);// 用户令牌集合半小时失效。
                logger.info("将token保存至用户token的集合中");
            }
        }
        return accessToken;
    }

    /**
     * 跳转登录页面
     */
    @RequestMapping("/sso/loginPage")
    private ModelAndView loginPage(@RequestParam("url") String url) {
        logger.info("原请求url: " + url);
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("url", url);
        return modelAndView;
    }


    /**
     * 强制用户下线操作重定向页面
     *
     * @param accessToken 用户登陆的token
     * @param url         用户原请求的URL
     */
    @RequestMapping("/sso/offline")
    public ModelAndView offline(@RequestParam("accessToken") String accessToken, @RequestParam("url") String url) {
        ModelAndView modelAndView = new ModelAndView("offline");
        modelAndView.addObject("accessToken", accessToken);
        modelAndView.addObject("url", url);
        return modelAndView;
    }

    /**
     * 强制用户下线
     *
     * @param response
     * @param accessToken  用户登陆的token
     * @param url          用户原请求的URL
     * @param forceOffline 1-强制下线
     */
    @RequestMapping("/sso/offlineOthers")
    @ResponseBody
    public String offlineOthers(HttpServletResponse response, String accessToken, String url, String forceOffline) {

        logger.info("accessToken:" + accessToken);
        logger.info("url:" + url);
        logger.info("forceOffline:" + forceOffline);
        if (!StringUtils.isEmpty(forceOffline)) {
            if (stringTemplate.hasKey(accessToken)) {
                String userId = stringTemplate.opsForValue().get(accessToken);
                String loginUserTokenSet = PasswordUtil.encodePwd(userId);
                Set<String> userTokenSet = stringTemplate.opsForSet().members(loginUserTokenSet);
                userTokenSet.remove(accessToken);
                // 先让其他用户的token过期。
                for (String token : userTokenSet) {
                    stringTemplate.delete(token);
                }
                String[] removeValueArr = userTokenSet.toArray(new String[userTokenSet.size()]);
                // 再移除tokenlist中的其他用户的所有的token。
                stringTemplate.opsForSet().remove(loginUserTokenSet, removeValueArr);
            }
        }
        try {
            Cookie cookie = new Cookie("accessToken", accessToken);
            cookie.setMaxAge(60 * 3);
            // 设置域
            // cookie.setDomain("huanzi.cn");
            //设置访问路径
            cookie.setPath("/");
            response.addCookie(cookie);
            //重定向到原先访问的页面
            response.sendRedirect(url);

            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 页面登录
     *
     * @param response 返回内容
     * @param username
     * @param password
     * @param url      原用户的请求的URL地址
     */
    @RequestMapping("/sso/login")
    @ResponseBody
    private String login(HttpServletResponse response, String username, String password, String url) {

        LoginTag hasAlreadyLogin = new LoginTag(false);

        String accessToken = checkUsernameAndPassword(username, password, hasAlreadyLogin);

        if (!StringUtils.isEmpty(accessToken)) {

            if (hasAlreadyLogin.isOn()) {
                // 同账号有其他用户在其他地方登陆了。
                try {
                    logger.info("有用户登陆，重定向到登出其他用户的界面！");
                    response.sendRedirect("http://localhost:10010/sso/offline?accessToken=" + accessToken
                            + "&url=" + url);
                    return null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                Cookie cookie = new Cookie("accessToken", accessToken);
                cookie.setMaxAge(60 * 3);
                //设置域
//                cookie.setDomain("huanzi.cn");
                //设置访问路径
                cookie.setPath("/");
                response.addCookie(cookie);
                //重定向到原先访问的页面
                response.sendRedirect(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        logger.info("登录失败");
        return "登录失败";
    }

    @Data
    class LoginTag {
        private boolean on;

        public LoginTag(boolean t) {
            on = t;
        }
    }
}
