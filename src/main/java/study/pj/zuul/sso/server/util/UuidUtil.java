package study.pj.zuul.sso.server.util;

import java.util.UUID;

public class UuidUtil {

    public static String getUuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
