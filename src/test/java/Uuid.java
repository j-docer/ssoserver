import java.util.UUID;

public class Uuid {
    public static void main(String[] args) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        System.out.println("uuid:" + uuid);
    }
}
